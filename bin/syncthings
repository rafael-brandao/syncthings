#! /usr/bin/env bash
set -Eeuo pipefail

_is_root() {
  [ "$(id -u)" -eq 0 ]
}

_systemctl() {
  if _is_root; then
    systemctl "$@"
  else
    systemctl --user "$@"
  fi
}

_setup() {
  # shellcheck source=/dev/null
  source shell-util-functions
  _require_script_execution

  # shellcheck source=/dev/null
  source shell-util-containers

  PROJECT_DIR=$(_git_repo_path)

  # shellcheck source=../.env
  source "$PROJECT_DIR/.env"

  PROFILE="${PROFILE-$(basename "$PROJECT_DIR")}"
  NETWORK_NAME="${PROFILE}_default"

  export SYNCTHING_IMAGE="${SYNCTHING_IMAGE-lscr.io/linuxserver/syncthing}"

  CONFIG_BASE_DIR="${XDG_CONFIG_HOME-$HOME/.config}/syncthings/profiles/$PROFILE"
  export CONTAINER1_CONFIG="${CONTAINER1_CONFIG-$CONFIG_BASE_DIR/$CONTAINER1/config}"
  export CONTAINER2_CONFIG="${CONTAINER2_CONFIG-$CONFIG_BASE_DIR/$CONTAINER2/config}"

  export CONTAINER1_NAME="${CONTAINER1_NAME-${PROFILE}_${CONTAINER1}}"
  export CONTAINER2_NAME="${CONTAINER2_NAME-${PROFILE}_${CONTAINER2}}"

  export CONTAINER1_DATA="${CONTAINER1_DATA-${HOME-/home/${USER}}}"
  export CONTAINER2_DATA="${CONTAINER2_DATA-${HOME-/home/${USER}}}"

  MAIN_SERVICE="${PROFILE}.service"
  CONTAINER1_SERVICE="${CONTAINER1_NAME}.service"
  CONTAINER2_SERVICE="${CONTAINER2_NAME}.service"

  SERVICE_DIR=$(
    if _is_root; then echo '/etc/systemd/system'; else echo "$HOME/.config/systemd/user"; fi
  )

  # transform required services into an array
  mapfile -td \, REQUIRED_SERVICES <<<"${REQUIRED_SERVICES-}"
  REQUIRED_SERVICES=("${REQUIRED_SERVICES[@]%$'\n'}") # drop '\n' added by '<<<'
}

_print_usage() {
  _info "Usage:
    syncthings [command]

Example:
    syncthings deploy

Arguments:
    command:  help | install | uninstall | start | stop | deploy | undeploy"
}

_query_containers() {
  __query() {
    podman ps \
      --all \
      --quiet \
      --format '{{.ID}}' \
      --filter label=io.podman.compose.project=$PROFILE
  }
  if [ "$#" -gt 0 ]; then __query "$@"; else __query; fi
}

_is_service_running() {
  [ "$(_systemctl is-active "$1" || true)" = 'active' ]
}

_is_service_installed() {
  [ "$(_systemctl --quiet list-units "$1" | awk '{print $1}')" = "$1" ]
}

_compose() {
  podman-compose --project-name $PROFILE "${@-}"
}

_compose_up() {
  _compose up --quiet-pull --detach
}

_start() {
  _is_service_running "$MAIN_SERVICE" || _systemctl start "$MAIN_SERVICE"
}

_stop() {
  __stop_containers() {
    _query_containers --filter status=running |
      xargs \
        --no-run-if-empty \
        --max-args 1 \
        --max-procs 2 \
        podman stop --time 20
  }
  ! _is_service_running "$MAIN_SERVICE" || _systemctl stop "$MAIN_SERVICE"
  __stop_containers
}

_uninstall() {
  _stop

  ! _is_service_installed "$MAIN_SERVICE" || _systemctl disable "$MAIN_SERVICE"
  ! _is_service_installed "$CONTAINER1_SERVICE" || _systemctl disable "$CONTAINER1_SERVICE"
  ! _is_service_installed "$CONTAINER2_SERVICE" || _systemctl disable "$CONTAINER2_SERVICE"

  rm --force "$SERVICE_DIR/$MAIN_SERVICE"
  rm --force "$SERVICE_DIR/$CONTAINER1_SERVICE"
  rm --force "$SERVICE_DIR/$CONTAINER2_SERVICE"
  _systemctl daemon-reload

  _query_containers | xargs --no-run-if-empty podman rm
  _remove_network "$NETWORK_NAME"
}

__process_configs() {

  __indirect() {
    echo "${!1}"
  }

  __container_name() {
    __indirect "CONTAINER${1}"
  }

  __container_config_path() {
    __indirect "CONTAINER${1}_CONFIG"
  }

  __container_config() {
    echo "$(__container_config_path "$1")/config.xml"
  }

  __container_configv0() {
    echo "$(__container_config_path "$1")/config.xml.v0"
  }

  __format() {
    xml format --omit-decl --indent-spaces 4 "$1"
  }

  __proccess_config() {
    declare container_config container_configv0 container_index other_device

    container_index="$1"
    container_config=$(__container_config "$container_index")
    container_configv0=$(__container_configv0 "$container_index")

    [ "$(md5sum "$container_config" | awk '{print $1}')" = "$(md5sum "$container_configv0" | awk '{print $1}')" ] || return 0

    other_device=$(
      other_container_index=$( (("$container_index" % 2 != 0)) && echo 2 || echo 1)
      other_container_name=$(__container_name "$other_container_index")
      other_container_config=$(__container_config "${other_container_index}")

      xml select \
        --template \
        --copy-of "/configuration/device[@name='$other_container_name']" \
        "$other_container_config"
    )

    __append_other_device() {
      # shellcheck disable=SC2116 disable=SC2086
      xml edit --append '/configuration/device[last()]' --type elem --name 'elem_copy' |
        sed "s|<elem_copy/>|$(echo $other_device)|"
    }

    __configure_gui() {
      xml edit --update '/configuration/gui/@tls' --value 'true' |   # enable gui tls
        xml edit --update '/configuration/gui/theme' --value 'black' # use gui black theme
    }

    __configure_options() {
      xml edit --update '//options/listenAddress' --value 'quic://0.0.0.0:22000' | # listen to quic connections only
        xml edit --update '//options/globalAnnounceEnabled' --value 'false' |      # disable global announcing
        xml edit --update '//options/relaysEnabled' --value 'false' |              # disable relaying
        xml edit --update '//options/natEnabled' --value 'false' |                 # disable NAT
        xml edit --update '//options/urAccepted' --value '1' |                     # accept user agreement
        xml edit --update '//options/urSeen' --value '3' |                         # disable user agreement alert
        xml edit --update '//options/startBrowser' --value 'false'                 # do not start browser
    }

    __configure_defaults() {
      declare container_name
      container_name=$(__container_name "$container_index")

      xml edit --update '//defaults/folder/@path' --value '/data' |                               # change default folder path to '/data'
        xml edit --insert '//defaults/device' --type attr --name 'name' --value "$container_name" # add device name
    }

    (
      xml edit --delete '/configuration/folder[@id="default"]' "$container_configv0" | # remove default sync
        __append_other_device |                                                        # append other device config
        __configure_gui |                                                              # configure user interface
        __configure_options |                                                          # configure options section
        __configure_defaults                                                           # configure defaults section
    ) >"$container_config"
  }

  __proccess_config '1'
  __proccess_config '2'
}

_install() {

  __generate_services() {

    __generate_main_service() {

      cat <<-EOF
# $MAIN_SERVICE
# $(LC_TIME=en_US.UTF-8 date '+%a %b %d %T %:::z %Y')

[Unit]
Description=Podman $MAIN_SERVICE
Requires=$CONTAINER1_SERVICE
After=$CONTAINER1_SERVICE
Requires=$CONTAINER2_SERVICE
After=$CONTAINER2_SERVICE
EOF

      for service_name in "${REQUIRED_SERVICES[@]}"; do
        cat <<-EOF
Requires=${service_name}.service
After=${service_name}.service
EOF
      done

      cat <<-EOF

[Service]
Type=oneshot
ExecStart=$(which true)
RemainAfterExit=yes

[Install]
WantedBy=default.target
EOF
    }

    __generate_sync_services() {
      __adjust_path() {
        __get_path() {
          declare -a binaries=('iptables' 'newuidmap' 'nsenter' 'slirp4netns') # missing binaries on path
          for binary in "${binaries[@]}"; do
            path+="$(dirname "$(which "$binary")"):"
          done
          echo "$path"
        }

        sed "/^\[Service\]/a Environment=PATH=$(__get_path)\${PATH}" | # add binaries to path
          sed -E "s|/bin/rm|$(which rm)|g"                             # adjust fixed paths to nix store
      }

      __make_dependency() {
        sed -E "/^Wants=.+$/i PartOf=$MAIN_SERVICE" |
          sed -E "/^PartOf=.+$/a Before=$MAIN_SERVICE" |
          sed -E "s/^WantedBy=.+$/WantedBy=$MAIN_SERVICE/"
      }

      podman generate systemd \
        --new \
        --name "$1" \
        --time 20 \
        --container-prefix '' \
        --separator '' |
        __adjust_path |
        __make_dependency
    }

    (__generate_main_service >"$SERVICE_DIR/$MAIN_SERVICE") &
    (__generate_sync_services "$CONTAINER1_NAME" >"$SERVICE_DIR/$CONTAINER1_SERVICE") &
    (__generate_sync_services "$CONTAINER2_NAME" >"$SERVICE_DIR/$CONTAINER2_SERVICE") &
    wait

    _systemctl daemon-reload
    _systemctl enable "$MAIN_SERVICE"
    _systemctl enable "$CONTAINER1_SERVICE"
    _systemctl enable "$CONTAINER2_SERVICE"
  }

  mkdir --parents "$CONFIG_BASE_DIR"

  _compose_up
  _stop
  (__process_configs) &
  (__generate_services) &
  wait
}

_undeploy() {
  _stop
  _uninstall
}

_deploy() {
  _undeploy
  _install
  _start
}

_main() {
  _setup

  trap 'popd 1>/dev/null' EXIT
  pushd "$PROJECT_DIR" 1>/dev/null

  local command="${1-help}"

  case "$command" in
  deploy) _deploy ;;
  undeploy) _undeploy ;;
  install) _install ;;
  uninstall) _uninstall ;;
  start) _start ;;
  stop) _stop ;;
  help) _print_usage ;;
  *)
    _error "$(basename "$0"): $LINENO: command '$command' not found"
    _print_usage
    exit 1
    ;;
  esac
}

_main "$@"
