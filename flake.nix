{
  description = "Syncthings";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-21.11";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    flake-utils.url = "github:numtide/flake-utils";

    shell-util-scripts = {
      url = "gitlab:rafael-brandao/shell-util-scripts";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, ... }@inputs:
    with inputs;

    let
      overlays = [ shell-util-scripts.overlay ];
      pkgsForSystem = system: import nixpkgs { inherit overlays system; };

    in flake-utils.lib.eachDefaultSystem (system: rec {
      stablePackages = import nixpkgs-stable { inherit system; };

      legacyPackages = pkgsForSystem system;

      devShell = with legacyPackages;
        mkShell {
          buildInputs = [
            bashInteractive
            coreutils
            findutils
            gawk
            iptables
            stablePackages.podman
            podman-compose
            ripgrep
            shell-util-containers
            shell-util-functions
            slirp4netns
            util-linux
            xmlstarlet
          ];

          shellHook = ''
            PATH="bin:$PATH"
            export PATH

            source shell-util-functions
            source shell-util-containers
          '';
        };

    });
}
